import {
  Query,
  DefaultValuePipe,
  ParseArrayPipe,
  Get,
  Controller,
} from '@nestjs/common';

@Controller()
export class AppController {
  @Get()
  findSomething(
    @Query(
      'tagIds',
      new DefaultValuePipe([123]),
      new ParseArrayPipe({ items: Number, optional: true }),
      // new ParseArrayPipe({ items: Number, optional: false }),
    )
    tagIds: number[],
  ): any {
    console.log('tagIds', tagIds);
    console.log();
    return {
      tagIds
    }
  }
}
